describe('stock-info', function() {
  var stockInfo = require('../src/stock-info');
  var a;

  describe('get', function() {
    it('should create a stock report based on ticker', function(done) {
      stockInfo.get('MSFT').then(function(result) {
        console.log('MSFT Report:', result);
        expect(result).toBeDefined();
        done();
      })
    });
  });

});
