'use strict';
const forkJoin = require('rxjs').forkJoin;
const prefs = require('./preferences.js');
const stockInfo = require('./stock-info.js');
const stockReport = require('./stock-report');

/**
 * Read a watch list of stock symbols from an S3 bucket, generate a 100 day high/low price summary for each symbol in watch list
 * and insert the report in to dynomodb table, finally returning the key of the report.
 */
module.exports.generateStockReport = async (event, context, callback) => {
  try {
    const preferences = await prefs.get();
    console.log('Preferences:', preferences);

    // Create a stock promise for each symbol in watch list
    const stockPromises = preferences.watchList.map(symbol => {
      return stockInfo.get(symbol);
    });

    // Wait for all promises to resolve, then we have our report
    const report = await forkJoin(stockPromises).toPromise();
    console.log('Report:', report);

    const result = await stockReport.put(JSON.stringify(report));
    console.log('Report Id:', result);

    callback(null, `Stock Report ${result} created successfully`);
  } catch (error) {
    console.error(error);
    callback(error);
  }
};
