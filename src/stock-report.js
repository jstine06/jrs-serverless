const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB();
const uuid = require('node-uuid');

/**
 * Insert a stock report into stock-report table
 */
module.exports.put = async reportString => {
  const key = uuid.v1();
  const params = {
    Item: {
      key: {
        S: key
      },
      report: {
        S: reportString
      }
    },
    ReturnConsumedCapacity: 'TOTAL',
    TableName: 'stock-report'
  };
  console.log('Create Item:', params.Item);
  return new Promise((resolve, reject) => {
    dynamodb.putItem(params, function(err, data) {
      if (err) { 
        reject(err);
      }
      resolve(key);
    });
  });
};
