'use strict';
const https = require('https');
const _ = require('lodash');

/**
 * Retrieve stock summary for ticker symbol
 */
module.exports.get = async symbol => {
  return await getStockQuote(symbol);
};

/**
 * Retreive 100 days worth of stock data for ticker input
 *
 * @param {*} symbol - stock ticker
 */
function getStockQuote(symbol) {
  const url = `https://www.alphavantage.co/query?outputsize=compact&function=TIME_SERIES_DAILY&symbol=${symbol}&interval=5min&apikey=VKBUI6342HA7520F&datatype=cs`;
  return new Promise((resolve, reject) => {
    https
      .get(url, res => {
        let str = '';

        res.on('data', data => {
          str += data;
        });

        res.on('end', () => {
          const summary = getStockSummary(JSON.parse(str.toString()));
          resolve(summary);
        });
      })
      .on('error', err => {
        reject(err);
      });
  });
}

/**
 * Take a alphavantage quote and turn it in to a high/low price summary
 *
 * @param {*} quote
 */
function getStockSummary(quote) {
  const symbol = quote['Meta Data']['2. Symbol'];
  const series = _.values(quote['Time Series (Daily)']);
  const high = _.maxBy(series, '2. high')['2. high'];
  const low = _.minBy(series, '3. low')['3. low'];
  const summary = {
    [symbol]: {
      last: series[0]['4. close'],
      high: high,
      low: low
    }
  };
  return summary;
}
