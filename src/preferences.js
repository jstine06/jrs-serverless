'use strict';
const AWS = require('aws-sdk');
const s3 = new AWS.S3();

/**
 * Retrieve preferences.json from preferences bucket
 */
module.exports.get = async () => {
  const bucketName = process.env.preferencesBucketName;
  const keyName = process.env.preferencesKey;

  return JSON.parse(await readFromS3(bucketName, keyName));
};

/**
 * Return file contents as a string
 *
 * @param {*} bucketName Name of S3 bucket
 * @param {*} filename Name of file to return data in string format
 */
async function readFromS3(bucketName, filename) {
  var params = { Bucket: bucketName, Key: filename };
  return new Promise((resolve, reject) => {
    s3.getObject(params, (err, data) => {
      if (err) reject(err);
      else resolve(data.Body.toString());
    });
  });
}
