# Overview

This application will turn a list of stock symobols in to a report containing the high and low price of each stock over the past 100 days.  Define the stocks you would like to generate a report for in content/preferences.json

# Getting Started

1. Install and [Configure AWS CLI](#awscli)
2. Run `npm install`
3. Set any preferred stock tickers in content/preferences.json
4. Run `npm run deploy`
5. Check DynamoDB table stock-report for generated report

<a name="awscli"></a>
# Configure AWS CLI

1. Install AWS CLI: https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html
2. In AWS Console, create a IAM user
    1. Add permissions for AWSCLoudFormationFullAccess, AmazonS3FullAccess, AmazonDynamoDBFullAccess, CloudWatchLogsFullAccess, IAMFullAccess, AWSLambdaFullAccess
    2. Keep note of the **access key id** and **secret access key**
3. Run 'aws configure'