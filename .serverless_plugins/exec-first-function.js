'use strict';
const AWS = require('aws-sdk');
const _ = require('lodash');

/**
 * This plugin will execute the first function in your serverless service after it is deployed.  
 * Execution will occur prior to finalization of deploy.
 */
class execFirstFunction {
  constructor(serverless, options) {
    this.serverless = serverless;
    this.options = options;

    this.commands = {
      exec: {
        commands: {
          first: {
            lifecycleEvents: ['run']
          }
        }
      }
    };

    this.hooks = {
      'before:deploy:finalize': () => this.serverless.pluginManager.run(['exec', 'first']),
      'exec:first:run': this.run.bind(this)
    };
  }

  run() {
    const _this = this;
    const firstFunction = _.keys(this.serverless.service.functions)[0];
    const params = {
      FunctionName: this.serverless.service.functions[firstFunction].name
    };

    const lambda = new AWS.Lambda({ region: _this.serverless.service.provider.region });
    lambda.invoke(params, function(err, data) {
      if (err) {
        _this.serverless.cli.log(err, err.stack);
      } else {
        _this.serverless.cli.log(data.Payload);
      }
    });
  }
}

module.exports = execFirstFunction;

